FROM ubuntu:16.04
MAINTAINER roel.deconinck@3e.eu

# Avoid interaction
ENV DEBIAN_FRONTEND noninteractive
ENV USER root
ENV SHELL /bin/bash

# Remove Ubuntu-specific line that exits the .bashrc when it is not interactive 
RUN /bin/bash -c "sed -i 's/^\[ \-z.*//g' ~/.bashrc"

# =========== Basic Configuration ======================================================
RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y sudo curl wget unzip \
      build-essential python python-dev python-wxgtk3.0 \
      liblapack-dev libatlas-base-dev libatlas-dev libblas-dev \
      python-setuptools make g++ cmake gfortran swig ant python-numpy m4 \
      python-scipy python-matplotlib cython python-lxml python-nose python-jpype \
      libboost-dev jcc git subversion zlib1g-dev pkg-config clang \
      libfreetype6 libfreetype6-dev pandoc libssl-dev libhdf5-dev libnetcdf-dev \
      libgeos-dev

ARG hdf5_version=1.10.1
ENV HDF5_VERSION=${hdf5_version} \
    HDF5_DIR_REL=hdf5-${hdf5_version} \
    HDF5_TAR=hdf5-${hdf5_version}.tar.gz \
    HDF5_DIR=/usr/local/hdf5-${hdf5_version} \
    HDF5_INCDIR=/usr/local/hdf5-${hdf5_version}/include \
    HDF5_LIBDIR=/usr/local/hdf5-${hdf5_version}/lib \
    LD_LIBRARY_PATH=/usr/local/hdf5-${hdf5_version}/lib:${LD_LIBRARY_PATH} \
    PATH=/usr/local/hdf5-${hdf5_version}:${PATH}
RUN cd ~ && \
    wget https://www.hdfgroup.org/ftp/HDF5/releases/hdf5-$(echo ${hdf5_version} | cut -d'.' -f 1,2)/${HDF5_DIR_REL}/src/${HDF5_TAR} && \
    tar xvfz ${HDF5_TAR} && \
    cd ${HDF5_DIR_REL} && \
    ./configure --prefix=${HDF5_DIR} --enable-shared --enable-hl && \
    make && \
    make install && \
    cd ~ && \
    rm ${HDF5_TAR} && \
    rm -rf ${HDF5_DIR_REL}

ENV CPPFLAGS=-I${HDF5_INCDIR} \
    LDFLAGS=-L${HDF5_LIBDIR}

ARG netcdf4_version=4.4.1.1
ENV NETCDF4_VERSION=${netcdf4_version} \
    NETCDF4_DIR_REL=netcdf-c-${netcdf4_version} \
    NETCDF4_TAR=v${netcdf4_version}.tar.gz \
    NETCDF4_DIR=/usr/local/netcdf-c-${netcdf4_version} \
    NETCDF4_INCDIR=/usr/local/netcdf-c-${netcdf4_version}/include \
    NETCDF4_LIBDIR=/usr/local/netcdf-c-${netcdf4_version}/lib \
    LD_LIBRARY_PATH=/usr/local/netcdf-c-${netcdf4_version}/lib:${LD_LIBRARY_PATH} \
    PATH=/usr/local/netcdf-c-${netcdf4_version}/bin:$PATH
RUN cd ~ && \
    wget https://github.com/Unidata/netcdf-c/archive/${NETCDF4_TAR} && \
    tar xvfz ${NETCDF4_TAR} && \
    cd ${NETCDF4_DIR_REL} && \
    ./configure --enable-netcdf-4 --enable-shared --enable-dap --prefix=${NETCDF4_DIR} && \
    make && \
    make install && \
    cd ~ && \
    rm ${NETCDF4_TAR} && \
    rm -rf ${NETCDF4_DIR_REL}

# JASPER
RUN cd ~ && \
    wget http://www.ece.uvic.ca/~mdadams/jasper/software/jasper-1.900.1.zip && \
    unzip -a jasper-1.900.1.zip && \
    cd jasper-1.900.1 && \
    ./configure --prefix=/usr/local/jasper-1.900.1 CFLAGS=-fPIC && \
    make && \
    make install && \
    cd ~ && \
    rm jasper-1.900.1.zip && \
    rm -rf jasper-1.900.1

# ECMWF GRIB API
RUN cd ~ && \
    wget https://software.ecmwf.int/wiki/download/attachments/3473437/grib_api-1.12.3.tar.gz && \
    tar xvfz grib_api-1.12.3.tar.gz && \
    cd grib_api-1.12.3 && \
    ./configure --prefix=/usr/local/grib_api-1.12.3 --with-netcdf=${NETCDF4_DIR} --with-jasper=/usr/local/jasper-1.900.1 CFLAGS=-fPIC && \
    make && \
    make install && \
    cd ~ && \
    rm grib_api-1.12.3.tar.gz && \
    rm -rf grib_api-1.12.3

RUN wget https://bootstrap.pypa.io/get-pip.py && \
    python get-pip.py && \
    rm get-pip.py && \
    pip install --upgrade pip && \
    pip install --upgrade --ignore-installed cython numexpr numpy scipy pandas matplotlib lxml paramiko html5lib

# ========= Add folders that will contain code before and after installation ==========
RUN mkdir -p /root/to_install && \
    mkdir -p /root/installed/Ipopt

# ========= Install JAVA ===============================================================
RUN apt-get install -y openjdk-8-jdk && \
    rm -rf /var/lib/apt/lists/*

# Define JAVA_HOME environment variable
ENV JAVA_HOME /usr/lib/jvm/java-8-openjdk-amd64
RUN echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" >> ~/.bashrc

# ======== Install BLAS and LAPACK =====================================================
RUN apt-get -y update && \
    apt-get install -y apt-utils && \
    apt-get install -y libblas-dev liblapack-dev

# ======== Install numpy, scipy, Matplotlib ============================================
RUN apt-get install -y pkgconf libpng-dev libfreetype6-dev && \
    pip install numpy && \
    apt-get install -y python-matplotlib && \
    pip install scipy

# ======== Start IPOPT installation ====================================================

# Retrieve and copy all the dependencies needed by Ipopt
WORKDIR /root/to_install/Ipopt
RUN wget http://www.coin-or.org/download/source/Ipopt/Ipopt-3.12.4.tgz
RUN tar xvf ./Ipopt-3.12.4.tgz
WORKDIR /root/to_install/Ipopt/Ipopt-3.12.4/ThirdParty/Blas
RUN ./get.Blas
WORKDIR /root/to_install/Ipopt/Ipopt-3.12.4/ThirdParty/Lapack
RUN ./get.Lapack
WORKDIR /root/to_install/Ipopt/Ipopt-3.12.4/ThirdParty/Mumps
RUN ./get.Mumps
WORKDIR /root/to_install/Ipopt/Ipopt-3.12.4/ThirdParty/Metis
RUN ./get.Metis

# Configure and compile Ipopt
WORKDIR /root/to_install/Ipopt/Ipopt-3.12.4/
RUN mkdir build
WORKDIR /root/to_install/Ipopt/Ipopt-3.12.4/build
RUN ../configure --prefix=/root/installed/Ipopt && \
    make && \
    make install

# ======== Start JModelica.org installation ===========================================0

# Install autoconf which is called by the casADi installation
RUN apt-get install -y autoconf

# Checkout the JModelica.org source code
RUN mkdir -p /root/to_install/JModelica
RUN mkdir -p /root/installed/JModelica

WORKDIR /root/to_install
#RUN svn co --ignore-externals https://svn.jmodelica.org/trunk JModelica
# Add svn externals file
#ADD svn.externals /root/svn.externals
# Update svn externals
#RUN svn propset svn:externals -F /root/svn.externals JModelica
RUN svn co https://svn.jmodelica.org/trunk JModelica

#WORKDIR /root/to_install/JModelica/external
#RUN svn co https://svn.jmodelica.org/assimulo/trunk Assimulo

WORKDIR /root/to_install/JModelica
RUN mkdir build
WORKDIR /root/to_install/JModelica/build
#RUN ../configure --prefix=/root/installed/JModelica \
#             --with-ipopt=/root/installed/Ipopt && \
#    sed -i 's/ ~/ \\\~/g' Makefile && \
#    make && \
#    make install && \
#    make install_casadi && \
#    make casadi_interface
RUN ../configure --prefix=/root/installed/JModelica \
             --with-ipopt=/root/installed/Ipopt && \
    make install && \
    make casadi_interface

# Add specific IPOPT solver MA27, can be used commercially
ADD libcoinhsl.so  /root/installed/Ipopt/lib/libhsl.so

# Define the environmental variables needed by JModelica
# JModelica.org supports the following environment variables:
#
# - JMODELICA_HOME containing the path to the JModelica.org installation
#   directory (again, without spaces or ~ in the path).
# - PYTHONPATH containing the path to the directory $JMODELICA_HOME/Python.
# - JAVA_HOME containing the path to a Java JRE or SDK installation.
# - IPOPT_HOME containing the path to an Ipopt installation directory.
# - LD_LIBRARY_PATH containing the path to the $IPOPT_HOME/lib directory
#   (Linux only.)
# - MODELICAPATH containing a sequence of paths representing directories
#   where Modelica libraries are located, separated by colons.
ENV JMODELICA_HOME /root/installed/JModelica
ENV IPOPT_HOME /root/installed/Ipopt
ENV CPPAD_HOME /root/installed/JModelica/ThirdParty/CppAD/
ENV SUNDIALS_HOME /root/installed/JModelica/ThirdParty/Sundials
ENV PYTHONPATH /mnt/shared/greyboxbuildings/python:\
/root/installed/JModelica/Python
ENV LD_LIBRARY_PATH /root/installed/Ipopt/lib/:\
/root/installed/JModelica/ThirdParty/Sundials/lib:\
/root/installed/JModelica/ThirdParty/CasADi/lib
ENV SEPARATE_PROCESS_JVM /usr/lib/jvm/java-8-openjdk-amd64/
ENV MODELICAPATH /root/installed/JModelica/ThirdParty/MSL:/root/modelica:/mnt/shared/FastBuildings

# ======== Clean apt-get ===========================================0

RUN apt-get clean
